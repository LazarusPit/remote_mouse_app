import 'package:flutter/material.dart';
import 'package:mobile_pc_app/helpers/networking.dart';
import 'package:mobile_pc_app/models/host_with_port.dart';

import 'helpers/notifier.dart';

class HostControlState extends StatefulWidget {
  final HostWithPort hostInfo;

  const HostControlState({Key? key, required this.hostInfo}) : super(key: key);

  @override
  State<HostControlState> createState() => _HostControlStateState();
}

class _HostControlStateState extends State<HostControlState> {
  final _formKey = GlobalKey<FormState>();

  late final Notifier _notifier = Notifier(context: context);
  late final Networking _networking;

  @override
  void initState() {
    super.initState();
    _networking = Networking(hostInfo: widget.hostInfo);
    _networking.connect().then((_) {}).catchError((error) {
      _notifier.displayUpdate("Unable to connect to host: $error");
      Navigator.pop(context);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  final TextEditingController _keyboardController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.hostInfo.toString()),
        ),
        body: Form(
          key: _formKey,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 250,
                    height: 250,
                    color: const Color.fromARGB(127, 0, 150, 255),
                    child: GestureDetector(
                      onTap: () async {
                        _notifier.displayUpdate("Tapped",
                            duration: UpdateDuration.short);
                        try {
                          await _networking.mouseTap();
                        } catch (err) {
                          _notifier.displayUpdate(
                              "Error while trying to send tap: $err");
                          Navigator.pop(context);
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: TextFormField(
                      validator: ((value) {
                        if (value!.isEmpty) return "";
                        return null;
                      }),
                      controller: _keyboardController,
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.text,
                      decoration:
                          const InputDecoration(labelText: "Keyboard input"),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      if (!_formKey.currentState!.validate()) return;
                      try {
                        _networking.keyboardInput(_keyboardController.text);
                      } catch (error) {
                        _notifier.displayUpdate(
                            "Error while trying to send input: $error",
                            duration: UpdateDuration.short);
                        Navigator.pop(context);
                      }
                    },
                    child: const Text("Send"),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
