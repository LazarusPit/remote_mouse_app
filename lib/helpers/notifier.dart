import 'package:flutter/material.dart';

enum UpdateDuration { short, long }

extension UpdateDurationExtension on UpdateDuration {
  Duration get value {
    switch (this) {
      case UpdateDuration.short:
        return const Duration(milliseconds: 500);
      case UpdateDuration.long:
        return const Duration(seconds: 3);
    }
  }
}

class Notifier {
  late BuildContext _context;
  Notifier({required BuildContext context}) {
    _context = context;
  }

  void displayUpdate(String status,
      {UpdateDuration duration = UpdateDuration.long}) {
    SnackBar snackBar = SnackBar(
      content: Text(status),
      duration: duration.value,
    );
    ScaffoldMessenger.of(_context).showSnackBar(snackBar);
  }
}
