import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:mobile_pc_app/models/host_with_port.dart';

class Networking {
  final HostWithPort hostInfo;

  Networking({required this.hostInfo});

  void printReceivedData(Uint8List value) => print(utf8.decode(value));

  Future<Socket> connect(
      {Function(Uint8List)? onResponse, Duration? timeToLive}) async {
    Socket socket = await Socket.connect(hostInfo.host, hostInfo.port,
        timeout: const Duration(seconds: 3));
    if (onResponse != null) {
      socket.listen(onResponse);
    }
    return socket;
  }

  Future<void> keyboardInput(String input) async {
    Socket socket = await connect();
    List<int> toSend = [48]; // utf8 for 0
    toSend.addAll(utf8.encode(input));
    socket.add(toSend);
    await Future.delayed(const Duration(seconds: 1));
    socket.close();
  }

  Future<void> mouseTap() async {
    Socket socket = await connect();
    socket.add([49, 48, 48]); // utf8 for 1, 0, 0
    await Future.delayed(const Duration(seconds: 1));
    socket.close();
  }
}
