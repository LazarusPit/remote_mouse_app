class HostWithPort {
  String host;
  int port;

  HostWithPort({required this.host, required this.port});

  @override
  String toString() {
    return "$host:$port";
  }

  @override
  bool operator ==(other) {
    return (other is HostWithPort) && other.host == host && other.port == port;
  }

  @override
  int get hashCode {
    int result = 17;
    result = 31 * result + host.hashCode;
    result = 31 * result + port.hashCode;
    return result;
  }
}
