import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mobile_pc_app/host_control.dart';
import 'package:mobile_pc_app/models/host_with_port.dart';

class HostList extends StatefulWidget {
  const HostList({Key? key}) : super(key: key);

  @override
  State<HostList> createState() => _HostListState();
}

class _HostListState extends State<HostList> {
  bool listsMatch<T>(List<T> first, List<T> second) {
    if (first.length != second.length) return false;
    for (var i = 0; i < first.length; i++) {
      if (first[i] != second[i]) return false;
    }
    return true;
  }

  @override
  void initState() {
    RawDatagramSocket.bind(InternetAddress.anyIPv4, 9999).then((socket) {
      socket.broadcastEnabled = true;
      socket.listen((event) {
        Datagram? datagram = socket.receive();
        if (datagram == null) return;
        List<int> expected = [1, 2, 3, 5, 8, 13];
        if (datagram.data.length != 8) return;
        if (!listsMatch(datagram.data.toList().sublist(0, 6), expected)) return;
        var hostInfo = HostWithPort(
            host: datagram.address.host,
            port: int.parse("${datagram.data[6]}${datagram.data[7]}"));

        if (_hosts.contains(hostInfo)) return;
        setState(() {
          _hosts.add(hostInfo);
        });
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  final List<HostWithPort> _hosts = [];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _hosts.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(_hosts[index].host),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => HostControlState(
                          hostInfo: _hosts[index],
                        )));
          },
        );
      },
    );
  }
}
