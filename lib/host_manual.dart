import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mobile_pc_app/helpers/networking.dart';
import 'package:mobile_pc_app/models/host_with_port.dart';

import 'helpers/notifier.dart';

class HostManualState extends StatefulWidget {
  const HostManualState({Key? key}) : super(key: key);

  @override
  State<HostManualState> createState() => _HostManualState();
}

class _HostManualState extends State<HostManualState> {
  final _formKey = GlobalKey<FormState>();

  late final Notifier _notifier = Notifier(context: context);

  final TextEditingController _ipController = TextEditingController();
  final TextEditingController _portController =
      TextEditingController(text: "8080");

  bool _connected = false;
  HostWithPort? connectedSocket;

  String get _status => _connected ? "Connected" : "No connection";

  Color get _statusColor => _connected
      ? const Color.fromARGB(255, 0, 255, 0)
      : const Color.fromARGB(255, 255, 0, 0);

  Future<void> connect() async {
    if (!_formKey.currentState!.validate()) return;
    var hostInfo = HostWithPort(
        host: _ipController.text, port: int.parse(_portController.text));
    if (connectedSocket != null) {
      if (hostInfo == connectedSocket) return;
    }
    var networking = Networking(hostInfo: hostInfo);
    _notifier.displayUpdate("Connecting...");
    bool connected = false;
    try {
      var socket = await networking.connect();
      socket.close();
      connected = true;
    } catch (error) {
      _notifier.displayUpdate("Unable to connect: $error");
    }
    setState(() {
      _connected = connected;
    });
  }

  final _txtRequiredField = "Required Field";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Manual host")),
        body: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(_status, style: TextStyle(color: _statusColor)),
                ],
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                SizedBox(
                  width: 100.0,
                  child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return _txtRequiredField;
                        }
                        if (!value.contains(RegExp(
                            r"^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"))) {
                          return "Invalid IP-Address";
                        }
                        return null;
                      },
                      textAlign: TextAlign.center,
                      controller: _ipController,
                      keyboardType: TextInputType.number,
                      decoration:
                          const InputDecoration(labelText: "IP Address*"),
                      onChanged: (_) {
                        setState(() {
                          _connected = false;
                        });
                      }),
                ),
                SizedBox(
                    width: 100.0,
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return _txtRequiredField;
                        }
                        if (!value.contains(RegExp(r'^[0-9]+$'))) {
                          return "Invalid port";
                        }
                        return null;
                      },
                      textAlign: TextAlign.center,
                      controller: _portController,
                      keyboardType: const TextInputType.numberWithOptions(
                          signed: false, decimal: false),
                      decoration: const InputDecoration(labelText: "Port*"),
                      onChanged: (value) {
                        setState(() {
                          _connected = false;
                        });
                      },
                    )),
              ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: connect,
                    child: const Text("Connect"),
                  )
                ],
              )
            ],
          ),
        ));
  }

  @override
  void dispose() {
    _ipController.dispose();
    super.dispose();
  }
}
