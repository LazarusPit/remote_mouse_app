import 'package:flutter/material.dart';

import 'package:mobile_pc_app/host_list.dart';
import 'package:mobile_pc_app/host_manual.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Available hosts'),
          actions: [
            Builder(
                // used to navigate via Navigator.push
                builder: (context) => IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const HostManualState()));
                    },
                    icon: const Icon(Icons.note_add_outlined)))
          ],
        ),
        body: const Center(
          child: HostList(),
        ),
      ),
    );
  }
}
